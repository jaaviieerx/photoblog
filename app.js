var path = require('path');
const express = require('express');
const app = express();
const AdminBroExpress = require('@admin-bro/express');
/*
var morgan = require('morgan');
var favicon = require('serve-favicon');
var ejs = require('ejs');
*/

app.use(express.static(path.join(__dirname, "client")));
app.set('view engine', 'ejs');

const { adminBro } = require('./client/admin/index');
const articlesRouter = require('./routes/articles.router');
const imagesRouter = require('./routes/images.router')
const router = AdminBroExpress.buildRouter(adminBro);
app.use(adminBro.options.rootPath, router);
//app.use(morgan('combined'));

//routers
app.get('/', articlesRouter);
app.get('/images', imagesRouter);

//error handling
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
if (app.get('env') === 'development') {
    app.use(function(err, req, res) {
        res.status(err.status || 500);
        res.render('errorPg', {
            message: err.message,
            error: err
        });
    });
};
app.use(function(err, req, res) {
    res.status(err.status || 500);
    res.render('errorPg', {
        message: err.message,
        error: {}
    });
});

module.exports = app;