var Images = require('../models/images');

imagesRouter = function(req, res, next){
  Images
  .findAll({
    /*where: {
      category: 'profile'
    },*/
    order: [
      ['img_order']
    ]
  })
  .then (function(image){
    res.render('', {
      image : image
    });
  })
};
module.exports = imagesRouter;