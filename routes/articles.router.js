var Articles = require('../models/articles');

articlesRouter = function(req, res, next){
  Articles
  .findAll({
    /*where: {
      category: 'profile'
    },*/
    order: [
      ['article_order']
    ]
  })
  .then (function(articles){
    res.render('', {
      articles : articles
    });
  })
};
module.exports = articlesRouter;