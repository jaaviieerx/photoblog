var Sequelize = require('sequelize');
var sequelize = require('./sequelize');

var Articles = sequelize.define('articles', {
  article_order: {
    type: Sequelize.INTEGER
  },
  category: {
    type: Sequelize.CHAR
  },
  title: {
    type: Sequelize.STRING
  },
  content: {
    type: Sequelize.TEXT('long')
    }
}, {
  freezeTableName: true
});

// var placeholderText = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada enim ac lobortis ultrices. Vestibulum in molestie est. Aliquam erat volutpat. Sed malesuada iaculis nisi, quis sagittis elit condimentum sit amet. Curabitur vulputate pharetra purus, et fringilla dui tincidunt eu. Nulla facilisi. In porta sagittis facilisis. Pellentesque tristique eros at sapien egestas, ut consectetur lectus varius. Donec sed tellus eu sapien dignissim tempus. Duis fermentum, sapien vel eleifend aliquet, felis massa mattis lacus, eu sollicitudin nulla nibh id velit. Donec fermentum tellus sed turpis dapibus, sit amet rhoncus magna tincidunt. Aliquam in vehicula sapien, non euismod ipsum. Vestibulum fermentum volutpat eros nec cursus. Nullam porttitor facilisis dolor, ut ultricies orci volutpat sed. Donec in tellus nec erat ultricies facilisis. \n Curabitur consequat nisl sed turpis fringilla tristique. In lobortis odio et ipsum pellentesque semper. Integer commodo, felis non sollicitudin cursus, dui ipsum feugiat lorem, at blandit nunc mi a urna. Aliquam sapien eros, pharetra vitae cursus eu, commodo in orci. Proin elementum finibus diam sed luctus. Curabitur sed quam id urna feugiat dignissim. Mauris eu bibendum arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquet arcu vel nulla rhoncus dignissim. \n Etiam arcu nulla, egestas nec auctor non, ullamcorper quis dui. Praesent iaculis nec tortor eget condimentum. Duis eget scelerisque nunc. Nunc posuere nulla id dui congue, a ultrices felis tempor. Nunc vel nunc nisi. Donec mollis eu magna id molestie. Sed dui massa, sollicitudin eu viverra id, mattis ullamcorper velit. Integer in pellentesque arcu, a ullamcorper velit. Mauris et lorem non ligula egestas elementum vel ac erat. Nam rutrum sed quam in pellentesque. Mauris a nunc convallis, finibus arcu vitae, consequat nunc. Sed eget tincidunt tellus, non finibus justo.';

/* defaults for references tab
Articles.sync({force: false}).then(function(){
  return Articles.create({
    article_order: 1,
    category: 'references',
    title: 'Reference 1',
    content: placeholderText
  })
});

Articles.sync({force: false}).then(function(){
  return Articles.create({
    article_order: 2,
    category: 'references',
    title: 'Reference 2',
    content: placeholderText
  })
});

Articles.sync({force: false}).then(function(){
  return Articles.create({
    article_order: 3,
    category: 'references',
    title: 'Reference 3',
    content: placeholderText
  })
});
*/

/* defaults for profile tab
Articles.sync({force: true}).then(function () {
  return Articles.create({
    article_order: 1,
    category: 'profile',
    title: 'About the Artist',
    content: placeholderText
  });
});
Articles.sync({force: true}).then(function () {
  return Articles.create({
    article_order: 2,
    category: 'profile',
    title: 'Achievements',
    content: placeholderText
  });
});

Articles.sync({force: true}).then(function () {
  return Articles.create({
    article_order: 3,
    category: 'profile',
    title: 'Timeline',
    content: placeholderText
  });
});
*/
module.exports = Articles;