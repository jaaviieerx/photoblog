var Sequelize = require('sequelize');
var sequelize = require('./sequelize');

var Images = sequelize.define('images', {
  img_order: {
    type: Sequelize.INTEGER
  },
  category: {
    type: Sequelize.CHAR
  },
  title: {
    type: Sequelize.STRING
  },
  content: {
    type: Sequelize.TEXT('long')
  },
  img_path: {
    type: Sequelize.STRING
  },
  image: {
    type: Sequelize.BLOB
  }
}, {
  freezeTableName: true
});
/*
Images.sync({force: true}).then(function(){
  return Images.create({
    img_order: 1,
    category: 'references',
    title: 'Reference 1',
    content: 'placeholderText',
    img_path: '/test/test.png'
  })
});*/

module.exports = Images;