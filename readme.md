# Introduction
An online portfolio website for Zi Yan by Fingertips Technologies with provided DNS. 

# Phase 1
A static tab-based image-heavy website 

# Phase 2
Content of texts and images to be edited by added editors. 

# Phase 3
Adding of dual-language, background music, and other minor features

# Packages
* Git
* HTML
* CSS
* JavaScript
* ExpressJs
* EJs
* NodeJs
* AdminBro [CMS]
* SequelizeJs
* MySQL

# To be implemented
* Bootstrap

# Potentially
* GatsbyJs + Photo Galleries, eg. Lightbox2, Joomla!, WordPress
* Quill

# Sitemap
https://xd.adobe.com/view/a94e7e3b-f8ce-4219-858b-caf1ea073537-fc1d/screen/10e64e02-4941-4aae-bdcd-47446bbc7ee2