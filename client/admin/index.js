const AdminBro = require('admin-bro');
const AdminBroSequelize = require('@admin-bro/sequelize');
const uploadFeature = require('@admin-bro/upload')
const sequelize = require('../../models/sequelize');

const Articles = require('../../models/articles');
const articlesOptions = {
    properties: {
        content: {
        type: 'richtext',
        props: {
            borderless: true, //
            quill: {
            // some custom props
            },
        },
        },
    },
};
const Images = require('../../models/images')
const imageOptions = {
    resources: [{
      resource: Images,
      options: { 
        properties: { 
              mimeType: [ 'image/jpeg', 'image/png' ] 
            } 
        },
      features: [uploadFeature({
        provider: { 
            local: { 
                bucket: './client/images' 
            } 
        },
        properties: {
            key: `images.id`,
            /*mimeType: `images.mimeType`,
            file: `images.image`,
            filePath: `images.img_path`*/
        }
      })]
    }],
    properties: {
        image: {
            components: {
                edit: AdminBro.bundle('./image.edit.js')
            }
        }
    }
  }

AdminBro.registerAdapter(AdminBroSequelize)

const adminBro = new AdminBro({
    databases: [sequelize],
    rootPath: '/admin',
    resources: [
        { resource: Articles, options: articlesOptions},
        { resource: Images, options: imageOptions}
    ],
    branding: {
        companyName: 'Welcome, Ziyan',
    }
});


module.exports.adminBro = adminBro;
